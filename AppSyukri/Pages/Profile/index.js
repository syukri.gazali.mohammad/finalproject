import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput } from 'react-native'

export default function Profile (){
    return(
        <View>
            <View style={{alignItems:'center', marginTop:50}}>
                <Text style={{fontSize:32,fontWeight:'bold'}}>My Profile</Text>
            </View>
            <View style={styles.box}>
                <Text style={{fontSize:26, fontWeight:'bold', marginBottom: 24}}>Syukri Gazali Mohammad</Text>
                <View style={styles.itemList}>
                    <Text style={styles.label}>Email</Text>
                    <Text style={styles.profileList}>syukri.gazali.mohammad@gmail.com</Text>
                </View>
                <View style={styles.itemList}>
                    <Text style={styles.label}>Telepon</Text>
                    <Text style={styles.profileList}>085801184456</Text>
                </View>
                <View style={styles.itemList}>
                    <Text style={styles.label}>Tempat tanggal lahir</Text>
                    <Text style={styles.profileList}>Jakarta, 4 November 1984</Text>
                </View>
                <View style={styles.itemList}>
                    <Text style={styles.label}>Alamat</Text>
                    <Text style={styles.profileList}>Jl Tanah Kusir II No.47 Kebayoran Lama - Jakarta Selatan</Text>
                </View>
                <View style={styles.itemList}>
                    <Text style={styles.label}>Pendidikan terakhir</Text>
                    <Text style={styles.profileList}>STIE Gotong Royong - Manajemen</Text>
                </View>
                <View style={styles.itemList}>
                    <Text style={styles.label}>Pendidikan non formal</Text>
                    <Text style={styles.profileList}>Sanber Code - React Native dasar</Text>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    box :{
        // flex :1 ,
        borderRadius: 20,
        marginHorizontal: 40,
        marginVertical: 10,
        paddingHorizontal: 30,
        paddingVertical: 20,
        height: 'auto',
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: '#f0f0f0',

    },
    itemList:{
        marginBottom:20
    },
    label:{
        fontSize: 14,
    },
    profileList :{
        fontSize: 18,
    },
})