import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput } from 'react-native'

const Login = ({navigation}) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isError, setIsError] = useState(false);

    const handleGoTo = screen => {
        navigation.navigate(screen)
    }
    const submit=()=> {
        const Data = {
            username, password
        }
        console.log(Data)

        if(password === "12345"){
            setIsError(false)
            console.log("Login Berhasil")
            navigation.navigate("Home", {username : username} )
        }else {
            console.log(alert('Login salah'))
            setIsError(true)
        }
    }
    return(
        <View style={styles.mainContainer}>
            <View style={styles.logoContainer}>
                <Image 
                    source={require('../../assets/images/logo-depan.jpg')}
                />
                <Text style={styles.namaApp}>Personal Portofolio</Text>
            </View>
            <View style={styles.inputContainer}>
                <View>
                    <Text style={styles.labelInput}>Username</Text>
                    <TextInput
                        style={styles.inputBox}
                        defaultValue="Masukkan email"
                        value = {username}
                        onChangeText={(value) => setUsername(value)}
                    />
                </View>
                <View style={{height: 20,}}></View>
                <View>
                    <Text style={styles.labelInput}>Password</Text>
                    <TextInput
                        style={styles.inputBox}
                        defaultValue="Masukkan password"
                        value = {password}
                        onChangeText= {(value) => setPassword(value)}
                    />
                </View>
                <View style={{flex:1, alignItems:'flex-end'}}>
                    <Text style={{fontWeight: 'bold',}}>Forgot Password ?</Text>
                </View>
                
            </View>
            <View style={styles.buttonContainer}>

                <TouchableOpacity style={styles.button} onPress={submit} >
                    <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
                        <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold',}}>Login</Text>
                    </View>
                </TouchableOpacity>
                <View style={{height:10,}}></View>
                <TouchableOpacity onPress={() => handleGoTo('Register')}  >
                    < Text style={{textDecorationLine: 'underline', fontWeight: 'bold',}}>Belum punya akun ? Register disini</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Login;

const styles = StyleSheet.create({
    mainContainer: {
        flex:1,
        marginTop:30,
    },
    logoContainer: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    namaApp:{
        fontSize: 26,
        fontWeight: 'bold',
        color: '#4B4A6D',
        marginTop: 15,
    },
    inputContainer:{
        flex: 1.5,
        paddingHorizontal: 40,
        justifyContent: 'center',
        // backgroundColor: 'blue',
    },
    labelInput:{
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    inputBox:{
        height: 40,
        color: '#1b1b1b',
        borderColor: '#E0E0E0',
        paddingHorizontal: 10,
        marginBottom:10,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 1,
        borderLeftWidth: 0,
        
    },
    buttonContainer :{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button :{
        height: 54,
        width: 320,
        borderRadius:25,
        backgroundColor: '#4B4A6D',
    },  

})