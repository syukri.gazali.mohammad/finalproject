import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput } from 'react-native'

const Register = ({navigation}) => {
    const handleGoTo = screen => {
        navigation.navigate(screen)
    }
    return(
        <View style={styles.mainContainer}>
            <View style={styles.logoContainer}>
                
                <Text style={styles.namaApp}>Registration</Text>
            </View>
            <View style={styles.inputContainer}>
                <View>
                    <Text style={styles.labelInput}>Username</Text>
                    <TextInput
                        style={styles.inputBox}
                        defaultValue="Masukkan email"
                    />
                </View>
                <View style={{height: 20,}}></View>
                <View>
                    <Text style={styles.labelInput}>Password</Text>
                    <TextInput
                        style={styles.inputBox}
                        defaultValue="Masukkan password"
                    />
                </View>
                <View style={{height: 20,}}></View>
                <View>
                    <Text style={styles.labelInput}>Ulangi Password</Text>
                    <TextInput
                        style={styles.inputBox}
                        defaultValue="Ketik ulang password"
                    />
                </View>
                
            </View>
            <View style={styles.buttonContainer}>

                <TouchableOpacity style={styles.button} onPress={() => handleGoTo('Home')} >
                    <View style={{flex:1, justifyContent: 'center', alignItems:'center'}}>
                        <Text style={{color: 'white', fontSize: 16, fontWeight: 'bold',}}>Daftar</Text>
                    </View>
                </TouchableOpacity>
                <View style={{height:10,}}></View>
                <TouchableOpacity onPress={() => handleGoTo('Login')} >
                <   Text style={{textDecorationLine: 'underline', fontWeight: 'bold',}}>Sudah punya akun ? Login disini</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Register;

const styles = StyleSheet.create({
    mainContainer: {
        flex:1,
        marginTop:30,
    },
    logoContainer: {
        flex: 0.5,
        justifyContent: 'center',
        alignItems: 'center',
    },
    namaApp:{
        fontSize: 26,
        fontWeight: 'bold',
        color: '#4B4A6D',
        marginTop: 15,
    },
    inputContainer:{
        flex: 2,
        paddingHorizontal: 40,
        justifyContent: 'center',
        // backgroundColor: 'blue',
    },
    labelInput:{
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 5,
    },
    inputBox:{
        height: 40,
        color: '#c1c1c1',
        borderColor: '#E0E0E0',
        paddingHorizontal: 10,
        marginBottom:10,
        borderTopWidth: 0,
        borderRightWidth: 0,
        borderBottomWidth: 1,
        borderLeftWidth: 0,
        
    },
    buttonContainer :{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    button :{
        height: 54,
        width: 320,
        borderRadius:25,
        backgroundColor: '#4B4A6D',
    },  

})