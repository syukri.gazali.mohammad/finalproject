import React from 'react';
import Home from './Home';
import Login from './Login';
import Register from './Register';
import Profile from './Profile';
import Splash from './Splash';
import Skills from './Skills'

export {Splash, Home, Login, Register, Profile, Skills}