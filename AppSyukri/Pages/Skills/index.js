import React from 'react';
import * as Progress from 'react-native-progress';
import { View, Text, Image, StyleSheet, TouchableOpacity, TextInput } from 'react-native'

export default function Profile (){
    return(
        <View>
           <View style={{alignItems:'center', marginTop:50}}>
                <Text style={{fontSize:32,fontWeight:'bold'}}>My Skills</Text>
            </View>

            <View style={styles.box}>
                <Text style={{fontSize:22, fontWeight:'bold', marginBottom:22}}>Skill</Text>
                <View style={styles.itemList}>
                    <Text style={styles.itemLabel}>Bahasa pemrograman</Text>
                    <View style={{flexDirection:'row'}}>
                        <Image source={require('../../assets/images/skill-js.png')} />
                        <View style={{marginLeft:10}}>
                            <Text>Basic Javascript</Text>
                            <Progress.Bar progress={0.3} width={200} height={12} borderRadius={10} color={'#54A3EC'} unfilledColor={"#D4EBFF"} borderColor={"#D4EBFF"} />
                        </View>
                    </View>
                </View>
                <View style={styles.itemList}>
                    <Text style={styles.itemLabel}>Framework</Text>
                    <View style={{flexDirection:'row'}}>
                        <Image source={require('../../assets/images/skill-react.png')} />
                        <View style={{marginLeft:10}}>
                            <Text>Basic React Native</Text>
                            <Progress.Bar progress={0.1} width={200} height={12} borderRadius={10} color={'#54A3EC'} unfilledColor={"#D4EBFF"} borderColor={"#D4EBFF"} />
                        </View>
                    </View>
                </View>
                <View style={styles.itemList}>
                    <Text style={styles.itemLabel}>Teknologi</Text>
                    <View style={{flexDirection:'row'}}>
                        <Image source={require('../../assets/images/skill-git.png')} />
                        <View style={{marginLeft:10}}>
                            <Text>Intermediate Git</Text>
                            <Progress.Bar progress={0.5} width={200} height={12} borderRadius={10} color={'#54A3EC'} unfilledColor={"#D4EBFF"} borderColor={"#D4EBFF"} />
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    box :{
        // flex :1 ,
        borderRadius: 20,
        marginHorizontal: 40,
        marginVertical: 10,
        paddingHorizontal: 30,
        paddingVertical: 20,
        height: 'auto',
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: '#f0f0f0',

    },
    itemList:{
        marginBottom:20
    },
    itemLabel:{
        fontSize: 20,
    },
    profileList :{
        fontSize: 18,
    },
})