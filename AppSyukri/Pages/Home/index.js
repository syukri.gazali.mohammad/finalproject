import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Text, View, Image, StyleSheet, ScrollView, TouchableOpacity, SafeAreaView} from 'react-native';
import Axios from 'axios'
import { FlatList } from 'react-native-gesture-handler';

const CovidStatus = ({ positif, dirawat, sembuh, meninggal }) => {
    return(
        <View style={{marginHorizontal:50,paddingTop:30}} >
            <Text style={{fontSize:24, justifyContent:'center'}}>Covid-19 Status di Indonesia</Text>
            <View style={{flexDirection:'row', justifyContent:'space-around' ,marginTop:20}}>
                <View style={{alignItems:'center'}}>
                    <Text>Positif :</Text>
                    <Text style={{fontSize:22, fontWeight:'bold',}}>{positif}</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <Text>Dirawat :</Text>
                    <Text style={{fontSize:22, fontWeight:'bold',}}>{dirawat}</Text>
                </View>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-around' ,marginTop:20}}>
                <View style={{alignItems:'center'}}>
                    <Text>Sembuh :</Text>
                    <Text style={{fontSize:22, fontWeight:'bold',}}>{sembuh}</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <Text>Meninggal :</Text>
                    <Text style={{fontSize:22, fontWeight:'bold',}}>{meninggal}</Text>
                </View>
            </View>
        </View>
    )
}

export default function Home({route, navigation, }) {
    const { username } = route.params;

    const handleGoTo = screen => {
        navigation.navigate(screen)
    }

    const [users, setUsers] = useState([]);

    useEffect(() => {
        getData();
    },[])

   const getData = () => {
       Axios.get('https://api.kawalcorona.com/indonesia')
       .then(res => {
           console.log('res: ', res)
           setUsers(res.data)
       })
   }

    return(
        <View>
            <View style={styles.mainContainer}>
                
                <SafeAreaView>
                    <ScrollView>

                        <View style={styles.nameTittle}>
                            <View style={{justifyContent: 'center', alignItems: 'center',}}>
                                <View>
                                    <Image  style={{borderRadius:12,}} source={require('../../assets/images/foto-pp.jpg')} />
                                </View>
                                <View style={{height:16,}}></View>
                                <Text style={{fontWeight:'bold', fontSize: 22,color: 'white',}}>Syukri Gazali Mohammad</Text>
                                <View style={{height:3,}}></View>
                                <Text style={{ fontSize: 16,color: 'white',}}>React Native Programer</Text>
                            </View>
                        </View>

                        {users.map(user => {
                            return <CovidStatus positif={user.positif} dirawat={user.dirawat} sembuh={user.sembuh} meninggal={user.meninggal} />
                        })}

                        <View style={{marginHorizontal:50, marginTop:20,}}>
                            <Text style={{fontSize:20, fontWeight:"bold", }}>Hi, {username}</Text>
                            <Text>Kamu sedang melihat portofolio saya, saya mohon dipergunakan dengan sebaik-baiknya, terima kasih... ^_^</Text>
                        </View>
                        
                        <View style={styles.sosmedWork}>

                            <View style={styles.box}>
                                <Text  style={styles.boxTittle}>Social media</Text>
                                <View style={{height:10}}></View>
                                <View style={{ flexDirection: 'row', justifyContent:'space-between'}}>
                                    <View style={{flexDirection: 'row'}}>
                                        <View>
                                            <Image source={require('../../assets/images/sosmed-icon-fb.png')}/>
                                        </View>
                                        <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row'}}>
                                        <View><Image source={require('../../assets/images/sosmed-icon-linkedin.png')}/></View>
                                        <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                                    </View>
                                </View>
                                <View style={{height:10}}></View>
                                <View style={{ flexDirection: 'row',  justifyContent:'space-between'}}>
                                    <View style={{ flexDirection: 'row',}}>
                                        <View>
                                            <Image source={require('../../assets/images/sosmed-icon-twitter.png')}/>
                                        </View>
                                        <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                                    </View>
                                    <View style={{ flexDirection: 'row',}}>
                                        <View><Image source={require('../../assets/images/sosmed-icon-ig.png')}/></View>
                                        <View><Text style={styles.sosmedText}>Syukri Gazali</Text></View>
                                    </View>
                                </View>
                            </View>

                            <View style={{height:10,}}></View>
                            
                            <View style={styles.box}>
                                <Text  style={styles.boxTittle}>My Work</Text>
                                <View style={{height:10}}></View>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <Image source={require('../../assets/images/mywork-icon-gitlab.png')} />
                                    <Text style={{marginLeft:10,fontSize:16, fontWeight:'bold'}}>Gitlab</Text>
                                </View>
                                <Text>https://gitlab.com/syukri.gazali.mohammad/bootcampreactnative</Text>
                            </View>

                        </View>


                        <View style={styles.navbarContainer}>
                            <TouchableOpacity onPress={() => handleGoTo('Profile')}>
                                <View style={styles.navbarItems}>
                                    <Text style={{color:'white'}}>Profile</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => handleGoTo('Skills')}>
                                <View style={styles.navbarItems}>
                                    <Text style={{color:'white'}}>Skills</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        </View>
    )

}



const styles = StyleSheet.create({
    mainContainer: {
        // flex:4,
        flexDirection:'column',
        marginTop:30,
    },
    navbarContainer :{
        // flex:1,
        height:120,
        // backgroundColor:'yellow',
        marginHorizontal:40,
        flexDirection: 'row',
        justifyContent:'space-evenly',
        alignItems:'center',
    },
    navbarItems :{
        height:50,
        width:150,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'#4B4A6D',
        borderRadius: 40,
    },

    nameTittle :{
        height: 260,
        backgroundColor: '#6A6FEA',
        justifyContent: 'center',
        alignItems: 'center',
    },
    sosmedWork :{
        paddingTop: 14,
    },
    box :{
        borderRadius: 20,
        marginHorizontal: 40,
        paddingHorizontal: 20,
        paddingTop: 10,
        paddingBottom: 40,
        borderWidth: 1,
        backgroundColor: 'white',
        borderColor: '#f0f0f0',
        height: "auto", 


    },
    boxTittle :{
        fontSize: 24,
        fontWeight: 'bold'
    },
    sosmedText :{
        fontSize:16,
        marginLeft:5,
    },
})