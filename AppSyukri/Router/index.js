import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Home, Login, Profile, Splash, Register, Skills } from '../Pages'

const Stack = createStackNavigator();

const Router = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen 
                name="Splash" 
                component={Splash} 
                options={{ 
                    headerShown: false,
                }}
            />
            <Stack.Screen 
            name="Login" 
            component={Login}
            options={{ 
                headerShown: false,
            }}
            />
            <Stack.Screen name="Home" component={Home} 
            options={{ 
                headerShown: false,
            }}/>
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Profile" component={Profile}/>
            <Stack.Screen name="Skills" component={Skills}/>
        </Stack.Navigator>
    )
}

export default Router;
